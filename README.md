# Golorconv

Written as a tiny test project. This is my first project written in
[Go](https://go.dev). It's a color code converter that takes an RGB tuple and
makes it into a both an HTML-like RGB888 string and an RGB555LE code suitable
for use on *GameBoy Color*.

## Example Output

```
$ ./golorconv
Golorconv - A basic color converter in Go.
Copyright 2023 Lethal Lisa
Licensed under the BSD 3-clause license.
Please input an RGB tuple or code in the form "(R,G,B)" or "#RRGGBB".
==> (56,89,100)
Input String: "(56,89,100)"
RGB Tuple   : (56, 89, 100)
RGB888/HTML : #385964
RGB555LE    : $3167
RGB555BE    : $1D6C
```

## Building

With `go` and `make` installed run:
```sh
make
sudo make install
```

## Licensing

This program is licensed under the [BSD 3-clause license](LICENSE).

## To Do

* [x] Write Makefile
* [ ] Other color formats for input or output
