.DEFAULT_GOAL := build

GOCMD  := go
BINARY := golorconv
PREFIX := /usr/local
BINDIR := ${PREFIX}/bin

.PHONY: test
test: clean build

.PHONY: build
build: $(BINARY)

.PHONY: clean
clean:
	$(GOCMD) clean

.PHONY: install
install: build
	install -d $(DESTDIR)$(BINDIR)
	install -m 0755 ./$(BINARY) $(DESTDIR)$(BINDIR)

.PHONY: uninstall
uninstall: clean
	rm -f $(DESTDIR)$(BINDIR)/$(BINARY)

$(BINARY): $(BINARY).go
	$(GOCMD) build -o $@
