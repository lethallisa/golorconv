package main
import (
	"errors"
	"flag"
	"fmt"
)

// Channels for RGB colors.
type rgbTuple struct {
	red, green, blue uint
}

// Methods for rgbTuple
func (t *rgbTuple) toRgb888() uint32 {
	return uint32(((t.red << 16) | (t.green << 8) | t.blue) & 0xFFFFFF)
}

func (t *rgbTuple) toRgb555le() uint16 {
	return uint16((((t.blue >> 3) << 10) | ((t.green >> 3) << 5) | (t.red >> 3)) & 0x7FFF)
}

func (t *rgbTuple) toRgb555be() uint16 {
	return uint16((((t.red >> 3) << 10) | ((t.green >> 3) << 5) | (t.blue >> 3)) & 0x7FFF)
}

func (t *rgbTuple) toTupleStr() string {
	return fmt.Sprintf("(%d, %d, %d)", t.red & 0xFF, t.green & 0xFF, t.blue & 0xFF)
}

func (t *rgbTuple) fromRgb888(rgb888 uint) {
	t.red = (rgb888 >> 16) & 0xFF
	t.green = (rgb888 >> 8) & 0xFF
	t.blue = rgb888 & 0xFF
}

func (t *rgbTuple) limitTo8bit() {
	t.red &= 0xFF
	t.green &= 0xFF
	t.blue &= 0xFF
}

// Macro function that checks if input was scanned properly.
func chkForScanErr (err error, nScanned int, nToScan int) error {
	if (err != nil) {
		return err
	}
	if (nScanned != nToScan) {
		return errors.New("Not enough input parameters")
	}
	return nil
}

func rgbFromString(strIn string) (rgbTuple, error) {
	var rInputType rune
	var uInColCode uint
	var rgbChan rgbTuple

	// Scan the first rune to determine input type.
	nScanned, scanErr := fmt.Sscanf(strIn, "%c", &rInputType)
	err := chkForScanErr(scanErr, nScanned, 1)
	if err != nil {
		return rgbChan, err
	}

	switch rInputType {
	case '#':
		nScanned, scanErr := fmt.Sscanf(strIn, "#%x", &uInColCode)
		err := chkForScanErr(scanErr, nScanned, 1)
		if err != nil {
			return rgbChan, err
		}
		rgbChan.fromRgb888(uInColCode)
	case '(':
		nScanned, scanErr := fmt.Sscanf(strIn, "(%d,%d,%d)", &rgbChan.red, &rgbChan.green, &rgbChan.blue)
		err := chkForScanErr(scanErr, nScanned, 3)
		if err != nil {
			return rgbChan, err
		}
		rgbChan.limitTo8bit()
	default:
		return rgbChan, errors.New("Invalid input type")
	}

	return rgbChan, nil
}

var strInput string

func init() {
	// Display license information.
	fmt.Println("Golorconv - A basic color converter in Go.\nCopyright 2023 Lethal Lisa\nLicensed under the BSD 3-clause license.")

	flag.StringVar(&strInput, "col", "", "The color code to convert.")
	flag.Parse()
}

func main() {
	// Get user input interactively if nothing is passed on the command line.
	if strInput == "" {

		// Prompt user for input.
		fmt.Println("Please input an RGB tuple or code in the form \"(R,G,B)\" or \"#RRGGBB\".")
		fmt.Print("==> ")

		_, err := fmt.Scanf("%s", &strInput)
		if err != nil {
			panic(err)
		}
	}

	// Process input into an rgbTuple struct.
	rgbChan, err := rgbFromString(strInput)
	if err != nil {
		panic(err)
	}

	// Display output.
	fmt.Printf("Input String: %q\n", strInput)
	fmt.Printf("RGB Tuple   : %s\n", rgbChan.toTupleStr())
	fmt.Printf("RGB888/HTML : #%X\n", rgbChan.toRgb888())
	fmt.Printf("RGB555LE    : $%X\n", rgbChan.toRgb555le())
	fmt.Printf("RGB555BE    : $%X\n", rgbChan.toRgb555be())
}
